package qrcodegen

import (
	//"fmt"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"math"

	"github.com/fogleman/gg"
	"gonum.org/v1/gonum/mat"
)

func applyColoredSquares(qr *image.RGBA) {
	// Change the color of squares as needed
	squareColor := color.RGBA{R: 0, G: 255, B: 0, A: 255}

	for y := 0; y < qr.Bounds().Dy(); y++ {
		for x := 0; x < qr.Bounds().Dx(); x++ {
			c := qr.At(x, y)
			_, _, _, a := c.RGBA()
			if a > 0 {
				qr.Set(x, y, squareColor)
			}
		}
	}
}

func applyGradient(qr *image.RGBA, startColor1, startColor2 color.Color) {
	gradient := gg.NewContext(qr.Bounds().Dx(), qr.Bounds().Dy())

	// Define the gradient colors and direction
	startColor := gg.NewLinearGradient(0, 0, float64(qr.Bounds().Dx()), float64(qr.Bounds().Dy()))
	startColor.AddColorStop(0, startColor1)
	startColor.AddColorStop(1, startColor2)

	gradient.SetFillStyle(startColor)

	// Draw the gradient rectangle
	gradient.DrawRectangle(0, 0, float64(qr.Bounds().Dx()), float64(qr.Bounds().Dy()))
	gradient.Fill()

	// Apply the gradient color to the QR code
	gradientImage := gradient.Image()

	for y := 0; y < qr.Bounds().Dy(); y++ {
		for x := 0; x < qr.Bounds().Dx(); x++ {
			qrColor := qr.At(x, y)
			r, g, b, _ := qrColor.RGBA()

			// Check if the QR code pixel is black
			if r == 0 && g == 0 && b == 0 {
				// Apply the gradient color to the black square
				gradientColor := gradientImage.At(x, y)
				qr.Set(x, y, gradientColor)
			}
		}
	}
}

func roundCorners(img image.Image, cornerRadius float64) *image.RGBA {
	srcBounds := img.Bounds()
	w, h := srcBounds.Dx(), srcBounds.Dy()
	rounded := image.NewRGBA(srcBounds)
	draw.Draw(rounded, rounded.Bounds(), &image.Uniform{C: color.Transparent}, image.ZP, draw.Src)

	squareSize := 0.1

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			pxColor := img.At(x, y)
			r, g, b, _ := pxColor.RGBA()
			gc := gg.NewContextForRGBA(rounded)

			// Check if the pixel color is black
			if r == 0 && g == 0 && b == 0 {
				gc.SetColor(color.Black)
				gc.DrawRoundedRectangle(float64(x)-squareSize/2, float64(y)-squareSize/2, squareSize, squareSize, cornerRadius)
				gc.Fill()
			} else if r == 65535 && g == 65535 && b == 65535 {
				gc.SetColor(color.White)
				gc.DrawRoundedRectangle(float64(x)-squareSize/2, float64(y)-squareSize/2, squareSize, squareSize, cornerRadius)
				gc.Fill()
			}
		}
	}

	return rounded
}


func roundCornersBIG(img image.Image, cornerRadius int) *image.RGBA {
    w := img.Bounds().Dx()
    h := img.Bounds().Dy()
    rounded := image.NewRGBA(img.Bounds())
    draw.Draw(rounded, rounded.Bounds(), &image.Uniform{C: color.Transparent}, image.ZP, draw.Src)
    gc := gg.NewContextForRGBA(rounded)
    gc.SetColor(color.White)
    gc.DrawRoundedRectangle(0, 0, float64(w), float64(h), float64(cornerRadius))
    gc.Fill()
    gc.SetColor(color.Black)
    gc.DrawImageAnchored(img, w/2, h/2, 0.5, 0.5)
    gc.Stroke()
    return rounded
}




// Convert image to grayscale intensity
func imageToIntensity(img image.Image) [][]int {
	bounds := img.Bounds()
	width, height := bounds.Dx(), bounds.Dy()

	intensity := make([][]int, width)
	for i := range intensity {
		intensity[i] = make([]int, height)
	}

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			r, g, b, _ := img.At(x, y).RGBA()
			gray := (299*r + 587*g + 114*b + 500) / 1000
			intensity[x][y] = int(gray)
		}
	}

	return intensity
}

// func edgeDetection(pixels *[][]color.Color){
func edgeDetection(img *image.RGBA) *image.RGBA {

	size := img.Bounds().Size()
	var pixels [][]color.Color
	//put pixels into two three two dimensional array
	for i := 0; i < size.X; i++ {
		var y []color.Color
		for j := 0; j < size.Y; j++ {
			y = append(y, img.At(i, j))
		}
		pixels = append(pixels, y)
	}

	//make image grey scale
	for x := 0; x < len(pixels); x++ {
		for y := 0; y < len(pixels[0]); y++ {
			r, g, b, a := pixels[x][y].RGBA()
			grey := 0.299*float64(r) + 0.587*float64(g) + 0.114*float64(b)
			c := uint8(grey + 0.5)
			pixels[x][y] = color.RGBA{
				R: c,
				G: c,
				B: c,
				A: uint8(a),
			}
		}
	}
	kernelx := mat.NewDense(3, 3, []float64{
		1, 0, 1,
		1, 3, 1,
		1, 0, 1,
	})
	kernely := mat.NewDense(3, 3, []float64{
		1, 1, 1,
		0, 3, 0,
		1, 1, 1,
	})
	//create two dimensional array to store intensities of each pixels
	intensity := make([][]int, len(pixels))
	for y := 0; y < len(intensity); y++ {
		intensity[y] = make([]int, len(pixels[0]))
	}
	//calculate intensities
	for i := 0; i < len(pixels); i++ {
		for j := 0; j < len(pixels[0]); j++ {
			colors := color.RGBAModel.Convert(pixels[i][j]).(color.RGBA)
			r := colors.R
			g := colors.G
			b := colors.B
			v := int(float64(float64(0.299)*float64(r) + float64(0.587)*float64(g) + float64(0.114)*float64(b)))
			intensity[i][j] = v
		}
	}
	//create new image
	newImage := make([][]color.Color, len(pixels))
	for i := 0; i < len(newImage); i++ {
		newImage[i] = make([]color.Color, len(pixels[0]))
	}

	for x := 1; x < len(pixels)-1; x++ {
		for y := 1; y < len(pixels[0])-1; y++ {
			var magx, magy int
			for a := 0; a < 3; a++ {
				for b := 0; b < 3; b++ {
					xn := x + a - 1
					yn := y + b - 1
					magx += intensity[xn][yn] * int(kernelx.At(a, b))
					magy += intensity[xn][yn] * int(kernely.At(a, b))
				}
			}
			p := int(math.Sqrt(float64(magx*magx + magy*magy)))
			newImage[x][y] = color.RGBA{
				R: uint8(p),
				G: uint8(p),
				B: uint8(p),
				A: 0,
			}
		}
	}

	eeeeeeimg := colorMatrixToRGBA(newImage)
	return eeeeeeimg
}

func colorMatrixToRGBA(colors [][]color.Color) *image.RGBA {
	width := len(colors)
	height := len(colors[0])

	rgba := image.NewRGBA(image.Rect(0, 0, width, height))

	for x := 1; x < width-1; x++ {
		for y := 1; y < height-1; y++ {
			fmt.Print(colors[x][y])
			rgba.Set(x, y, colors[x][y])
		}
	}

	return rgba
}
package qrcodegen

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"os"

	"github.com/nfnt/resize"
	"github.com/skip2/go-qrcode"
)

type CodeController struct{}

func NewCodeController() CodeController {
	return CodeController{}
}

// Generate a QR code with an icon in the middle
func (g *CodeController) GetIcon(content string, logoPath string, outputPath string, outputName string) {

	var (
		bgImg    image.Image
		offset   image.Point
		iconFile *os.File
		iconImg  image.Image
	)

	// Content to be encoded
	// Highest Recovery Level
	qrCode, err := qrcode.New(content, qrcode.Highest)
	if err != nil {
		fmt.Println(err)
		//return nil, err
		return
	}

	// Assigning qrcode image to the variable bgImg for further processing
	qrCode.DisableBorder = false
	bgImg = qrCode.Image(256)

	// Importing the logo file
	iconFile, err = os.Open(logoPath)
	if err != nil {
		fmt.Println(err)
		//return nil, err
		return
	}
	defer iconFile.Close()

	iconImg, _, err = image.Decode(iconFile)
	if err != nil {
		fmt.Println(err)
		//return nil, err
		return
	}

	// Modify the size of the logo image
	iconImg = resize.Resize(60, 60, iconImg, resize.Lanczos3)

	// Get the size of the background image
	b := bgImg.Bounds()
	//Center logo to QR code image
	offset = image.Pt((b.Max.X-iconImg.Bounds().Max.X)/2, (b.Max.Y-iconImg.Bounds().Max.Y)/2)

	m := image.NewRGBA(b)

	green := color.RGBA{R: 0, G: 255, B: 0, A: 255} // Green
	blue := color.RGBA{R: 0, G: 0, B: 255, A: 255}  // Blue

	//applyColoredSquares(m, green, blue)

	draw.Draw(m, b, bgImg, image.Point{X: 0, Y: 0}, draw.Src)
	m = roundCorners(m, 1)
	applyGradient(m, green, blue)
	

	draw.Draw(m, iconImg.Bounds().Add(offset), iconImg, image.Point{X: 0, Y: 0}, draw.Over)
	//m = edgeDetection(m)

	//save image
	result := fmt.Sprintf("%s%s", outputPath, outputName)
	SaveImage(result, m, 100)

	//errsave := SaveImage(result, m)
	/*
		if errsave != nil {
			fmt.Println(errsave)
		}
		pngurl := "/static/images/q2.png"
		html := "<img src='" + pngurl + "' />"
		c.Header("Content-Type", "text/html; charset=utf-8")
		c.String(200, html)
	*/
	return
}
